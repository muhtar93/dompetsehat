package com.dompet.dompetsehat

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.view.View
import android.widget.*
import com.dompet.dompetsehat.handler.DatabaseHandler
import com.dompet.dompetsehat.model.InOut
import java.util.*

class Masuk_Keluar_Uang : AppCompatActivity() {
    private lateinit var spinner: Spinner
    private lateinit var etNominal: EditText
    private lateinit var etDate: EditText
    private lateinit var spinnerTunai: Spinner
    private lateinit var etNote: EditText
    private lateinit var btnSave: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_masuk_keluar_uang)

        spinner = findViewById(R.id.spinner)
        etNominal = findViewById(R.id.editTextNominal)
        etDate = findViewById(R.id.dateManage)
        spinnerTunai = findViewById(R.id.spinnerTunai)
        etNote = findViewById(R.id.etNote)
        btnSave = findViewById(R.id.btnSave)

        val inouts = resources.getStringArray(R.array.inout)
        val tunai = resources.getStringArray(R.array.tunai)

        val adapters = ArrayAdapter(this,
            android.R.layout.simple_spinner_item, inouts)
        spinner.adapter = adapters

        spinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>,
                                        view: View, position: Int, id: Long) {

            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }

        val adapter = ArrayAdapter(this,
            android.R.layout.simple_spinner_item, tunai)
        spinnerTunai.adapter = adapter

        spinnerTunai.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>,
                                        view: View, position: Int, id: Long) {}
            override fun onNothingSelected(parent: AdapterView<*>) {}
        }

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, { view, year, monthOfYear, dayOfMonth ->
            var newMonth =  ""
            if (monthOfYear < 10) {
                newMonth = "0${monthOfYear + 1}"
            } else {
                newMonth = "${monthOfYear + 1}"
            }

            var newDay =  ""
            if (dayOfMonth < 10) {
                newDay = "0${dayOfMonth}"
            } else {
                newDay = "${dayOfMonth}"
            }

            etDate.setText("$year-${newMonth}-$newDay")
        }, year, month, day)

        etDate.setInputType(InputType.TYPE_NULL);
        etDate.setOnClickListener {
            dpd.show()
        }

        btnSave.setOnClickListener {
            addInOut()
        }
    }

    fun addInOut() {
        val id = (1..100).shuffled().last()

        val databaseHandler = DatabaseHandler(this)
        if(etNominal.text.trim()!="" && etDate.text.trim()!="" && etNote.text.trim()!=""
            && etNominal.text.toString().trim()!=""){
            var nominal = 0
            if (spinner.selectedItemId.toInt() == 0) {
                nominal = etNominal.text.toString().toInt()
            } else {
                nominal = -etNominal.text.toString().toInt()
            }

            val status = databaseHandler.addInOut(
                InOut(
                    id,
                    spinner.selectedItemId.toInt(),
                    nominal,
                    etDate.text.toString(),
                    spinnerTunai.selectedItemId.toInt(),
                    etNote.text.toString(),
                    intent.getIntExtra("id", 0)
                )
            )

            if(status > -1){
                Toast.makeText(applicationContext,"Input data berhasil", Toast.LENGTH_LONG).show()
                etNominal.text.clear()
                etNote.text.clear()
                etDate.text.clear()
                val returnIntent = Intent()
                setResult(RESULT_CANCELED, returnIntent)
                finish()
            }
        }else{
            Toast.makeText(applicationContext,"Input data gagal", Toast.LENGTH_LONG).show()
        }

    }
}