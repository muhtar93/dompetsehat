package com.dompet.dompetsehat

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.dompet.dompetsehat.handler.DatabaseHandler

class Profil : AppCompatActivity() {
    lateinit var sharedPreferences: SharedPreferences
    lateinit var tvName: TextView
    lateinit var tvUsername: TextView
    lateinit var tvAddress: TextView
    lateinit var tvBirthday: TextView
    lateinit var tvGender: TextView
    lateinit var btnUpdateProfile: Button
    lateinit var databaseHandler: DatabaseHandler
    var PREFS_KEY = "prefs"
    var USERNAME_KEY = "username"
    var username = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profil)

        tvName = findViewById(R.id.tvName)
        tvUsername = findViewById(R.id.tvUsername)
        tvAddress = findViewById(R.id.tvAddress)
        tvBirthday = findViewById(R.id.tvBirthday)
        tvGender = findViewById(R.id.tvGender)
        btnUpdateProfile = findViewById(R.id.btnUpdate)

        sharedPreferences = getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE)
        username = sharedPreferences.getString(USERNAME_KEY, null)!!

        databaseHandler = DatabaseHandler(this)
        val id = databaseHandler.viewUser(username).get(0).id
        val name = databaseHandler.viewUser(username).get(0).name
        val usernames = databaseHandler.viewUser(username).get(0).username
        val password = databaseHandler.viewUser(username).get(0).username
        val address = databaseHandler.viewUser(username).get(0).address
        val birthday = databaseHandler.viewUser(username).get(0).birthday
        val gender = databaseHandler.viewUser(username).get(0).gender

        tvName.text = name
        tvUsername.text = "Nama Pengguna : $usernames"
        tvAddress.text = "Alamat : $address"
        tvBirthday.text = "Tanggal Lahir : $birthday"

        if (gender == 0) {
            tvGender.text = "Jenis Kelamin : Laki-Laki"
        } else {
            tvGender.text = "Jenis Kelamin : Perempuan"
        }

        btnUpdateProfile.setOnClickListener {
            val intent = Intent(this, UpdateProfile::class.java)
            intent.putExtra("id", id)
            intent.putExtra("name", name)
            intent.putExtra("username", usernames)
            intent.putExtra("password", password)
            intent.putExtra("address", address)
            intent.putExtra("birthday", birthday)
            intent.putExtra("gender", gender)
            startActivity(intent)
        }
    }

}