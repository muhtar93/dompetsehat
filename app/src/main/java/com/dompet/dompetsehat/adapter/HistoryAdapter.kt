package com.dompet.dompetsehat.adapter

import android.app.Activity
import android.support.design.widget.BottomSheetDialog
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.dompet.dompetsehat.R

// class HistoryAdapter adalah turunan dari class ArrayAdapter
class HistoryAdapter(private val context: Activity,
                     private val id: Array<String>,
                     private val name: Array<String>,
                     private val date: Array<String>,
                     private val note: Array<String>
)
    : ArrayAdapter<String>(context, R.layout.list_item, name) {

    // function bawaan dari class ArrayAdapter
    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = context.layoutInflater
        val rowView = inflater.inflate(R.layout.list_item, null, true)

        val idText = rowView.findViewById(R.id.tvDate) as TextView
        val nameText = rowView.findViewById(R.id.tvNominal) as TextView
        val detail = rowView.findViewById(R.id.test) as ImageView

        // detail riwayat
        detail.setOnClickListener {
            val dialog = BottomSheetDialog(context)
            val view = context.layoutInflater.inflate(R.layout.bottom_sheet_filter, null)
            dialog.setContentView(view)
            val tvDate = view.findViewById<TextView>(R.id.detailDate)
            val tvNominal = view.findViewById<TextView>(R.id.detailNominal)
            val tvNote = view.findViewById<TextView>(R.id.detailNote)
            tvDate.text = "Tanggal : ${date[position]}"
            tvNominal.text = "Nominal : ${name[position]}"
            tvNote.text = "Keterangan : ${note[position]}"
            dialog.show()
        }

        idText.text = "${date[position]}"
        nameText.text = "${name[position]}"
        return rowView
    }
}