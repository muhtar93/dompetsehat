package com.dompet.dompetsehat

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.dompet.dompetsehat.handler.DatabaseHandler

class Login : AppCompatActivity() {
    private lateinit var btnLogin: Button
    private lateinit var username: EditText
    private lateinit var password: EditText
    private lateinit var register: TextView
    var PREFS_KEY = "prefs"
    var USERNAME_KEY = "username"
    var PASSWORD_KEY = "password"
    private lateinit var sharedPreferences: SharedPreferences
    var keyUsername = ""
    var keyPassword = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btnLogin = findViewById(R.id.btnLogin)
        username = findViewById(R.id.editTextUsername)
        password = findViewById(R.id.editTextPassword)
        register = findViewById(R.id.textRegister)

        sharedPreferences = getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE)
        keyUsername = sharedPreferences.getString(USERNAME_KEY, "").toString()
        keyPassword = sharedPreferences.getString(PASSWORD_KEY, "").toString()

        btnLogin.setOnClickListener {
            val databaseHandler = DatabaseHandler(this)
            if (databaseHandler.login(username.text.toString(),password.text.toString())) {
                val editor: SharedPreferences.Editor = sharedPreferences.edit()
                editor.putString(USERNAME_KEY, username.text.toString())
                editor.putString(PASSWORD_KEY, password.text.toString())
                editor.apply()
                beranda(it)
            } else {
                Toast.makeText(this, "Maaf anda gagal login", Toast.LENGTH_LONG).show()
            }
        }

        register.setOnClickListener {
            val intent = Intent(this@Login, Daftar::class.java)
            startActivity(intent)
        }
    }

    fun beranda(view: View?) {
        val intent = Intent(this@Login, Beranda::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }
}