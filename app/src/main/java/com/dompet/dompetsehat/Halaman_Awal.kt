package com.dompet.dompetsehat

import android.database.sqlite.SQLiteDatabase
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class Halaman_Awal : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_halaman_awal)
    }

    fun login(view: View?) {
        val intent = Intent(this@Halaman_Awal, Login::class.java)
        startActivity(intent)
    }
}

