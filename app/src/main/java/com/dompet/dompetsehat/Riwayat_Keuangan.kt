package com.dompet.dompetsehat

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.design.widget.BottomSheetDialog
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.widget.*
import com.dompet.dompetsehat.adapter.HistoryAdapter
import com.dompet.dompetsehat.handler.DatabaseHandler
import com.dompet.dompetsehat.model.InOut
import com.dompet.dompetsehat.util.convertRupiah
import java.util.*
import kotlin.collections.ArrayList

class Riwayat_Keuangan : AppCompatActivity() {
    private lateinit var recyclerView: ListView
    private lateinit var listInOut: ArrayList<InOut>
    private lateinit var tvIn: TextView
    private lateinit var tvOut: TextView
    private lateinit var mulai: EditText
    private lateinit var akhir: EditText
    private lateinit var cash: Spinner
    private lateinit var inOutSpinner: Spinner
    private lateinit var btnFilter: Button
    private lateinit var databaseHandler: DatabaseHandler
    lateinit var sharedPreferences: SharedPreferences
    var id = 0
    var username = ""
    var PREFS_KEY = "prefs"
    var USERNAME_KEY = "username"

    @SuppressLint("Range")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_riwayat_keuangan)

        tvIn = findViewById(R.id.tvIn)
        tvOut = findViewById(R.id.tvOut)
        mulai = findViewById(R.id.mulai)
        akhir = findViewById(R.id.akhir)
        akhir = findViewById(R.id.akhir)
        cash = findViewById(R.id.tunai_nontunai)
        inOutSpinner = findViewById(R.id.masuk_keluar)
        btnFilter = findViewById(R.id.btnFilter)

        databaseHandler = DatabaseHandler(this)

        sharedPreferences = getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE)
        username = sharedPreferences.getString(USERNAME_KEY, null)!!
        id = databaseHandler.viewUser(username).get(0).id

        tvIn.text = "Total Pemasukan : "
        tvOut.text = "Total Pengeluaran : "

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, { view, year, monthOfYear, dayOfMonth ->
            var months = ""
            if (monthOfYear < 10) {
                months = "0${monthOfYear+1}"
            } else {
                months = "${monthOfYear+1}"
            }

            var day = ""
            if (dayOfMonth < 10) {
                day = "0$dayOfMonth"
            } else {
                day = "$dayOfMonth"
            }
            mulai.setText("$year-$months-$day")
        }, year, month, day)

        mulai.setInputType(InputType.TYPE_NULL);
        mulai.setOnClickListener {
            dpd.show()
        }

        val dpdEnd = DatePickerDialog(this, { view, year, monthOfYear, dayOfMonth ->
            var months = ""
            if (monthOfYear < 10) {
                months = "0${monthOfYear+1}"
            } else {
                months = "${monthOfYear+1}"
            }

            var day = ""
            if (dayOfMonth < 10) {
                day = "0$dayOfMonth"
            } else {
                day = "$dayOfMonth"
            }
            akhir.setText("$year-$months-$day")
        }, year, month, day)

        akhir.setInputType(InputType.TYPE_NULL);
        akhir.setOnClickListener {
            dpdEnd.show()
        }

        val tunai = resources.getStringArray(R.array.tunai)
        val inOut = resources.getStringArray(R.array.inout)

        val adapter = ArrayAdapter(this,
            android.R.layout.simple_spinner_item, tunai)
         cash.adapter = adapter

        val adapterInOut = ArrayAdapter(this,
            android.R.layout.simple_spinner_item, inOut)
        inOutSpinner.adapter = adapterInOut

        recyclerView = findViewById(R.id.rvRiwayat)

        listInOut = ArrayList()

        btnFilter.setOnClickListener {
            val emp: List<InOut> = databaseHandler.viewInOut(mulai.text.toString(),
                akhir.text.toString(), cash.selectedItemId.toInt(), inOutSpinner.selectedItemId.toInt())
            val empArrayId = Array<String>(emp.size){"0"}
            val empArrayName = Array<String>(emp.size){"null"}
            val empArrayEmail = Array<String>(emp.size){"null"}
            val arrayNote = Array<String>(emp.size){"null"}
            var index = 0
            for(e in emp){
                empArrayId[index] = e.id.toString()
                empArrayName[index] = e.nominal.toString()
                empArrayEmail[index] = e.date
                arrayNote[index] = e.note
                index++
            }

            val myListAdapter = HistoryAdapter(this,empArrayId,empArrayName,empArrayEmail, arrayNote)
            recyclerView.adapter = myListAdapter

            val sumIn = databaseHandler.sumIn(mulai.text.toString(),
                akhir.text.toString(), cash.selectedItemId.toInt())
            val sumOut = databaseHandler.sumOut(mulai.text.toString(),
                akhir.text.toString(), cash.selectedItemId.toInt())
            val sumPlanning = databaseHandler.sumPlanning(id)
            val totalPengeluaran = sumOut - sumPlanning
            val totalPemasukan = sumIn + totalPengeluaran

            tvIn.text = "Total Pemasukan : ${totalPemasukan.convertRupiah()}"


             tvOut.text = "Total Pengeluaran : ${totalPengeluaran.convertRupiah().drop(1)}"
        }

    }
}