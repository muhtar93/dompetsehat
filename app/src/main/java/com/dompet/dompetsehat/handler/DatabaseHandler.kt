package com.dompet.dompetsehat.handler

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.dompet.dompetsehat.model.*

class DatabaseHandler(context: Context): SQLiteOpenHelper(context,DATABASE_NAME,null,DATABASE_VERSION) {
    companion object {
        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "DSDatabase"
        // user
        private val TABLE_USER = "user"
        private val KEY_ID = "id"
        private val KEY_NAME = "name"
        private val KEY_USERNAME = "username"
        private val KEY_PASSWORD = "password"
        private val KEY_ADDRESS = "address"
        private val KEY_BIRTHDAY = "birthday"
        private val KEY_GENDER = "gender"
        // inout
        private val TABLE_IN_OUT = "inout"
        private val KEY_IN_OUT_ID = "id"
        private val KEY_TYPE = "type"
        private val KEY_NOMINAL = "nominal"
        private val KEY_DATE = "date"
        private val KEY_IS_TUNAI = "is_tunai"
        private val KEY_NOTE = "notee"
        private val KEY_IN_OUT_USER_ID = "user_id"
        // planning
        private val TABLE_PLANNING = "planning"
        private val KEY_PLANNING_ID = "id"
        private val KEY_PLANNING_DATE = "date"
        private val KEY_PLANNING_NOMINAL = "nominal"
        private val KEY_PLANNING_NOTE = "note"
        private val KEY_IS_DONE = "is_done"
        private val KEY_PLANNING_USER_ID = "user_id"
        // asset
        private val TABLE_ASSET = "asset"
        private val KEY_ASSET_ID = "id"
        private val KEY_ASSET_DATE = "date"
        private val KEY_ASSET_TYPE = "nominal"
        private val KEY_PRICE = "note"
        private val KEY_ASSET_USER_ID = "user_id"
        // saving
        private val TABLE_SAVING = "saving"
        private val KEY_SAVING_ID = "id"
        private val KEY_SAVING_DATE = "date"
        private val KEY_SAVING_NOMINAL = "nominal"
        private val KEY_SAVING_NOTE = "note"
        private val KEY_SAVING_USER_ID = "user_id"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        // table user
        val CREATE_USER_TABLE = ("CREATE TABLE " + TABLE_USER + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_NAME + " TEXT,"
                + KEY_USERNAME + " TEXT,"
                + KEY_PASSWORD + " TEXT,"
                + KEY_ADDRESS + " TEXT,"
                + KEY_BIRTHDAY + " TEXT,"
                + KEY_GENDER + " INTEGER" +
        ")")
        // table inout
        val CREATE_INOUT_TABLE = ("CREATE TABLE " + TABLE_IN_OUT + "("
                + KEY_IN_OUT_ID + " INTEGER PRIMARY KEY,"
                + KEY_TYPE + " INTEGER,"
                + KEY_NOMINAL + " INTEGER,"
                + KEY_DATE + " TEXT,"
                + KEY_IS_TUNAI + " INTEGER,"
                + KEY_NOTE + " TEXT,"
                + KEY_IN_OUT_USER_ID + " INTEGER" +
                ")")
        // table planning
        val CREATE_PLANNING_TABLE = ("CREATE TABLE " + TABLE_PLANNING + "("
                + KEY_PLANNING_ID + " INTEGER PRIMARY KEY,"
                + KEY_PLANNING_DATE + " TEXT,"
                + KEY_PLANNING_NOMINAL + " INTEGER,"
                + KEY_PLANNING_NOTE + " TEXT,"
                + KEY_IS_DONE + " INTEGER,"
                + KEY_PLANNING_USER_ID + " INTEGER" +
                ")")
        // table asset
        val CREATE_ASSET_TABLE = ("CREATE TABLE " + TABLE_ASSET + "("
                + KEY_ASSET_ID + " INTEGER PRIMARY KEY,"
                + KEY_ASSET_DATE + " TEXT,"
                + KEY_ASSET_TYPE + " TEXT,"
                + KEY_PRICE + " INTEGER,"
                + KEY_ASSET_USER_ID + " INTEGER" +
                ")")
        // table saving
        val CREATE_SAVING_TABLE = ("CREATE TABLE " + TABLE_SAVING + "("
                + KEY_SAVING_ID + " INTEGER PRIMARY KEY,"
                + KEY_SAVING_DATE + " TEXT,"
                + KEY_SAVING_NOMINAL + " INTEGER,"
                + KEY_SAVING_NOTE + " TEXT,"
                + KEY_SAVING_USER_ID + " INTEGER" +
                ")")

        db?.execSQL(CREATE_USER_TABLE)
        db?.execSQL(CREATE_INOUT_TABLE)
        db?.execSQL(CREATE_PLANNING_TABLE)
        db?.execSQL(CREATE_ASSET_TABLE)
        db?.execSQL(CREATE_SAVING_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS " + TABLE_USER)
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_IN_OUT)
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLANNING)
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ASSET)
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SAVING)
        onCreate(db)
    }

    // add function
    fun addUser(user: User):Long{
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_ID, user.id)
        contentValues.put(KEY_NAME, user.name)
        contentValues.put(KEY_USERNAME, user.username )
        contentValues.put(KEY_PASSWORD, user.password )
        contentValues.put(KEY_ADDRESS, user.address )
        contentValues.put(KEY_BIRTHDAY, user.birthday )
        contentValues.put(KEY_GENDER, user.gender )
        val success = db.insert(TABLE_USER, null, contentValues)
        db.close()
        return success
    }

    fun addInOut(inOut: InOut):Long{
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_ID, inOut.id)
        contentValues.put(KEY_TYPE, inOut.type)
        contentValues.put(KEY_DATE, inOut.date)
        contentValues.put(KEY_NOMINAL, inOut.nominal)
        contentValues.put(KEY_IS_TUNAI, inOut.isTunai)
        contentValues.put(KEY_NOTE, inOut.note )
        contentValues.put(KEY_IN_OUT_USER_ID, inOut.userId)
        val success = db.insert(TABLE_IN_OUT, null, contentValues)
        db.close()
        return success
    }

    fun addPlanning(planning: Planning):Long{
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_PLANNING_ID, planning.id)
        contentValues.put(KEY_PLANNING_DATE, planning.date )
        contentValues.put(KEY_PLANNING_NOMINAL, planning.nominal )
        contentValues.put(KEY_PLANNING_NOTE, planning.note )
        contentValues.put(KEY_IS_DONE, planning.isDone )
        contentValues.put(KEY_PLANNING_USER_ID, planning.userId )
        val success = db.insert(TABLE_PLANNING, null, contentValues)
        db.close()
        return success
    }

    fun addAsset(asset: Asset):Long{
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_ASSET_ID, asset.id)
        contentValues.put(KEY_ASSET_DATE, asset.date )
        contentValues.put(KEY_ASSET_TYPE, asset.type )
        contentValues.put(KEY_PRICE, asset.price )
        contentValues.put(KEY_ASSET_USER_ID, asset.userId)
        val success = db.insert(TABLE_ASSET, null, contentValues)
        db.close()
        return success
    }

    fun addSaving(saving: Saving):Long{
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_SAVING_ID, saving.id)
        contentValues.put(KEY_SAVING_DATE, saving.date )
        contentValues.put(KEY_SAVING_NOMINAL, saving.nominal )
        contentValues.put(KEY_SAVING_NOTE, saving.note )
        contentValues.put(KEY_SAVING_USER_ID, saving.userId )
        val success = db.insert(TABLE_SAVING, null, contentValues)
        db.close()
        return success
    }

    // view function
    @SuppressLint("Range")
    fun viewInOut(dateStart: String, dateEnd: String, istunai: Int, type: Int):ArrayList<InOut>{
        val userList:ArrayList<InOut> = ArrayList<InOut>()

        val selectQuery = "SELECT  * FROM $TABLE_IN_OUT WHERE $KEY_DATE BETWEEN '$dateStart' AND '$dateEnd' AND $KEY_IS_TUNAI = $istunai AND $KEY_TYPE = $type"
        val db = this.readableDatabase
        var cursor: Cursor? = null
        try{
            cursor = db.rawQuery(selectQuery, null)
        }catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return ArrayList()
        }

        var id: Int
        var type: Int
        var date: String
        var nominal: Int
        var isTunai: Int
        var note: String
        var userId: Int
        if (cursor.moveToFirst()) {
            do {
                id = cursor.getInt(cursor.getColumnIndex(KEY_IN_OUT_ID))
                date = cursor.getString(cursor.getColumnIndex(KEY_DATE))
                type = cursor.getInt(cursor.getColumnIndex(KEY_TYPE))
                nominal = cursor.getInt(cursor.getColumnIndex(KEY_NOMINAL))
                isTunai = cursor.getInt(cursor.getColumnIndex(KEY_IS_TUNAI))
                note = cursor.getString(cursor.getColumnIndex(KEY_NOTE))
                userId = cursor.getInt(cursor.getColumnIndex(KEY_IN_OUT_USER_ID))
                val inout = InOut(id, type, nominal, date, isTunai, note, userId)
                userList.add(inout)
            } while (cursor.moveToNext())
        }

        return userList
    }

    @SuppressLint("Range")
    fun viewManagement():ArrayList<Planning>{
        val userList:ArrayList<Planning> = ArrayList<Planning>()
        val selectQuery = "SELECT  * FROM $TABLE_PLANNING WHERE $KEY_IS_DONE = 0"
        val db = this.readableDatabase
        var cursor: Cursor? = null
        try{
            cursor = db.rawQuery(selectQuery, null)
        }catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return ArrayList()
        }

        var id: Int
        var date: String
        var nominal: Int
        var note: String
        var isDone: Int
        var userId: Int
        if (cursor.moveToFirst()) {
            do {
                id = cursor.getInt(cursor.getColumnIndex(KEY_PLANNING_ID))
                date = cursor.getString(cursor.getColumnIndex(KEY_PLANNING_DATE))
                nominal = cursor.getInt(cursor.getColumnIndex(KEY_PLANNING_NOMINAL))
                note = cursor.getString(cursor.getColumnIndex(KEY_PLANNING_NOTE))
                isDone = cursor.getInt(cursor.getColumnIndex(KEY_IS_DONE))
                userId = cursor.getInt(cursor.getColumnIndex(KEY_PLANNING_USER_ID))
                val planning = Planning(id, date, nominal, note, isDone, userId)
                userList.add(planning)
            } while (cursor.moveToNext())
        }

        return userList
    }

    @SuppressLint("Range")
    fun viewSaving():ArrayList<Saving>{
        val userList:ArrayList<Saving> = ArrayList<Saving>()
        val selectQuery = "SELECT  * FROM $TABLE_SAVING"
        val db = this.readableDatabase
        var cursor: Cursor? = null
        try{
            cursor = db.rawQuery(selectQuery, null)
        }catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return ArrayList()
        }

        var id: Int
        var date: String
        var nominal: Int
        var note: String
        var userId: Int
        if (cursor.moveToFirst()) {
            do {
                id = cursor.getInt(cursor.getColumnIndex(KEY_PLANNING_ID))
                date = cursor.getString(cursor.getColumnIndex(KEY_PLANNING_DATE))
                nominal = cursor.getInt(cursor.getColumnIndex(KEY_PLANNING_NOMINAL))
                note = cursor.getString(cursor.getColumnIndex(KEY_PLANNING_NOTE))
                userId = cursor.getInt(cursor.getColumnIndex(KEY_PLANNING_USER_ID))
                val planning = Saving(id, date, nominal, note, userId)
                userList.add(planning)
            } while (cursor.moveToNext())
        }

        return userList
    }

    @SuppressLint("Range")
    fun viewAsset():ArrayList<Asset>{
        val userList:ArrayList<Asset> = ArrayList<Asset>()
        val selectQuery = "SELECT  * FROM $TABLE_ASSET"
        val db = this.readableDatabase
        var cursor: Cursor? = null
        try{
            cursor = db.rawQuery(selectQuery, null)
        }catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return ArrayList()
        }

        var id: Int
        var date: String
        var type: String
        var price: Int
        var userId: Int
        if (cursor.moveToFirst()) {
            do {
                id = cursor.getInt(cursor.getColumnIndex(KEY_ASSET_ID))
                date = cursor.getString(cursor.getColumnIndex(KEY_ASSET_DATE))
                type = cursor.getString(cursor.getColumnIndex(KEY_ASSET_TYPE))
                price = cursor.getInt(cursor.getColumnIndex(KEY_PRICE))
                userId = cursor.getInt(cursor.getColumnIndex(KEY_ASSET_USER_ID))
                val asset = Asset(id, date, type, price, userId)
                userList.add(asset)
            } while (cursor.moveToNext())
        }

        return userList
    }

    @SuppressLint("Range")
    fun viewUser(username: String):ArrayList<User>{
        val userList:ArrayList<User> = ArrayList<User>()
        val selectQuery = "SELECT  * FROM $TABLE_USER WHERE $KEY_USERNAME = '$username'"
        val db = this.readableDatabase
        var cursor: Cursor? = null
        try{
            cursor = db.rawQuery(selectQuery, null)
        }catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return ArrayList()
        }

        var id: Int
        var name: String
        var username: String
        var password: String
        var address: String
        var birthday: String
        var gender: Int
        if (cursor.moveToFirst()) {
            do {
                id = cursor.getInt(cursor.getColumnIndex(KEY_IN_OUT_ID))
                name = cursor.getString(cursor.getColumnIndex(KEY_NAME))
                username = cursor.getString(cursor.getColumnIndex(KEY_USERNAME))
                password = cursor.getString(cursor.getColumnIndex(KEY_PASSWORD))
                address = cursor.getString(cursor.getColumnIndex(KEY_ADDRESS))
                birthday = cursor.getString(cursor.getColumnIndex(KEY_BIRTHDAY))
                gender = cursor.getInt(cursor.getColumnIndex(KEY_GENDER))
                val user = User(id, name, username, password, address, birthday, gender)
                userList.add(user)
            } while (cursor.moveToNext())
        }

        return userList
    }

    @SuppressLint("Range")
    fun viewManageById(id: Int):ArrayList<Planning>{
        val userList:ArrayList<Planning> = ArrayList<Planning>()
        val selectQuery = "SELECT  * FROM $TABLE_PLANNING WHERE $KEY_PLANNING_ID = '$id'"
        val db = this.readableDatabase
        var cursor: Cursor? = null
        try{
            cursor = db.rawQuery(selectQuery, null)
        }catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return ArrayList()
        }

        var id: Int
        var date: String
        var nominal: Int
        var note: String
        var isDone: Int
        var userId: Int
        if (cursor.moveToFirst()) {
            do {
                id = cursor.getInt(cursor.getColumnIndex(KEY_PLANNING_ID))
                date = cursor.getString(cursor.getColumnIndex(KEY_PLANNING_DATE))
                nominal = cursor.getInt(cursor.getColumnIndex(KEY_PLANNING_NOMINAL))
                note = cursor.getString(cursor.getColumnIndex(KEY_PLANNING_NOTE))
                isDone = cursor.getInt(cursor.getColumnIndex(KEY_IS_DONE))
                userId = cursor.getInt(cursor.getColumnIndex(KEY_PLANNING_USER_ID))
                val planning = Planning(id, date, nominal, note, isDone, userId)
                userList.add(planning)
            } while (cursor.moveToNext())
        }

        return userList
    }

    // Login function
    @SuppressLint("Range")
    fun login(username: String, password: String):Boolean{
        val columns = arrayOf(KEY_ID)
        val db = this.readableDatabase
        val selection = "$KEY_USERNAME = ? AND $KEY_PASSWORD = ?"
        val selectionArgs = arrayOf(username, password)

        val cursor = db.query(TABLE_USER, //Table to query
            columns, //columns to return
            selection, //columns for the WHERE clause
            selectionArgs, //The values for the WHERE clause
            null,  //group the rows
            null, //filter by row groups
            null) //The sort order
        val cursorCount = cursor.count
        cursor.close()
        db.close()

        if (cursorCount > 0)
            return true
        return false
    }

    // Update function
    fun updateUser(user: User):Int{
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_ID, user.id)
        contentValues.put(KEY_NAME, user.name)
        contentValues.put(KEY_USERNAME, user.username )
        contentValues.put(KEY_PASSWORD, user.password )
        contentValues.put(KEY_ADDRESS, user.address )
        contentValues.put(KEY_BIRTHDAY, user.birthday )
        contentValues.put(KEY_GENDER, user.gender )
        val success = db.update(TABLE_USER, contentValues,"id="+user.id,null)
        db.close()
        return success
    }

    fun updateManage(planning: Planning):Int{
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_PLANNING_ID, planning.id)
        contentValues.put(KEY_PLANNING_DATE, planning.date)
        contentValues.put(KEY_PLANNING_NOMINAL, planning.nominal )
        contentValues.put(KEY_PLANNING_NOTE, planning.note )
        contentValues.put(KEY_IS_DONE, planning.isDone )
        val success = db.update(TABLE_PLANNING, contentValues,"id="+planning.id,null)
        db.close()
        return success
    }

    // Delete function
    fun deleteManagement(planning: Planning):Int{
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_PLANNING_ID, planning.id)
        val success = db.delete(TABLE_PLANNING,"id="+planning.id,null)
        db.close()
        return success
    }

    fun deleteAsset(asset: Asset):Int{
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_ASSET_ID, asset.id)
        val success = db.delete(TABLE_ASSET,"id="+asset.id,null)
        db.close()
        return success
    }

    // Sum function
    fun sumInOut(userId: Int): Int {
        var result = 0
        val db = this.writableDatabase
        val cursor = db.rawQuery(
            "select sum($KEY_NOMINAL) from $TABLE_IN_OUT WHERE $KEY_IN_OUT_USER_ID = $userId",
            null
        )
        if (cursor.moveToFirst()) result = cursor.getInt(0)
        cursor.close()
        db.close()
        return result
    }

    fun sumIn(dateStart: String, dateEnd: String, istunai: Int): Int {
        var result = 0
        val db = this.writableDatabase
        val cursor = db.rawQuery(
            "select sum($KEY_NOMINAL) from $TABLE_IN_OUT WHERE $KEY_TYPE = 0 AND $KEY_DATE BETWEEN '$dateStart' AND '$dateEnd' AND $KEY_IS_TUNAI = $istunai",
            null
        )
        if (cursor.moveToFirst()) result = cursor.getInt(0)
        cursor.close()
        db.close()
        return result
    }

    fun sumOut(dateStart: String, dateEnd: String, istunai: Int): Int {
        var result = 0
        val db = this.writableDatabase
        val cursor = db.rawQuery(
            "select sum($KEY_NOMINAL) from $TABLE_IN_OUT WHERE $KEY_TYPE = 1 AND $KEY_DATE BETWEEN '$dateStart' AND '$dateEnd' AND $KEY_IS_TUNAI = $istunai",
            null
        )
        if (cursor.moveToFirst()) result = cursor.getInt(0)
        cursor.close()
        db.close()
        return result
    }

    fun sumAsset(): Int {
        var result = 0
        val db = this.writableDatabase
        val cursor = db.rawQuery(
            "select sum($KEY_PRICE) from $TABLE_ASSET",
            null
        )
        if (cursor.moveToFirst()) result = cursor.getInt(0)
        cursor.close()
        db.close()
        return result
    }

    fun sumSaving(): Int {
        var result = 0
        val db = this.writableDatabase
        val cursor = db.rawQuery(
            "select sum($KEY_SAVING_NOMINAL) from $TABLE_SAVING",
            null
        )
        if (cursor.moveToFirst()) result = cursor.getInt(0)
        cursor.close()
        db.close()
        return result
    }

    fun sumPlanning(userId: Int): Int {
        var result = 0
        val db = this.writableDatabase
        val cursor = db.rawQuery(
            "select sum($KEY_PLANNING_NOMINAL) from $TABLE_PLANNING WHERE $KEY_IS_DONE = 1 AND $KEY_PLANNING_USER_ID = $userId",
            null
        )
        if (cursor.moveToFirst()) result = cursor.getInt(0)
        cursor.close()
        db.close()
        return result
    }
}