package com.dompet.dompetsehat

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Switch
import android.widget.Toast
import com.dompet.dompetsehat.handler.DatabaseHandler
import com.dompet.dompetsehat.model.Planning
import com.dompet.dompetsehat.model.User

class UpdateManagementActivity : AppCompatActivity() {
    private lateinit var databaseHandler: DatabaseHandler
    private lateinit var swicthManage: Switch
    private lateinit var dateManage: EditText
    private lateinit var nominalManage: EditText
    private lateinit var noteManage: EditText
    private lateinit var btnUpdate: Button
    var username = ""
    var userId = 0
    var PREFS_KEY = "prefs"
    var USERNAME_KEY = "username"
    lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_management)

        databaseHandler = DatabaseHandler(this)

        sharedPreferences = getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE)
        username = sharedPreferences.getString(USERNAME_KEY, null)!!
        userId = databaseHandler.viewUser(username).get(0).id

        val id = intent.getIntExtra("id", 0)

        swicthManage = findViewById(R.id.switchManage)
        dateManage = findViewById(R.id.dateManage)
        nominalManage = findViewById(R.id.nominalManage)
        noteManage = findViewById(R.id.noteManage)
        btnUpdate = findViewById(R.id.btnUpdateManage)

        val idManage = databaseHandler.viewManageById(id).get(0).id
        val date = databaseHandler.viewManageById(id).get(0).date
        val nominal = databaseHandler.viewManageById(id).get(0).nominal
        val note = databaseHandler.viewManageById(id).get(0).note
        val isDone = databaseHandler.viewManageById(id).get(0).isDone

        dateManage.setText(date)
        nominalManage.setText(nominal.toString())
        noteManage.setText(note)

        if (isDone == 0) {
            swicthManage.isChecked = false
        } else {
            swicthManage.isChecked = true
        }

        btnUpdate.setOnClickListener {
            val updateDate = dateManage.text.toString()
            val updateNominal = nominalManage.text.toString()
            val updateNote = noteManage.text.toString()
            var updateIsDone = 0

            if (swicthManage.isChecked) {
                updateIsDone = 1
            }

            if(updateDate.trim()!="" && updateNominal.trim()!="" && updateNote.trim()!== ""){
                val status = databaseHandler.updateManage(
                    Planning(id,updateDate, updateNominal.toInt(), updateNote, updateIsDone, userId)
                )
                if(status > -1){
                    Toast.makeText(applicationContext,"Profile berhasil diupdate", Toast.LENGTH_LONG).show()
                    val returnIntent = Intent()
                    setResult(RESULT_CANCELED, returnIntent)
                    finish()
                }
            }else{
                Toast.makeText(applicationContext,"id or name or email cannot be blank", Toast.LENGTH_LONG).show()
            }
        }
    }
}