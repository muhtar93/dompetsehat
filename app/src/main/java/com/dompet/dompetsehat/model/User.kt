package com.dompet.dompetsehat.model

// class model adalah class yang berfungsi untuk menampung data, class User adalah class model
// untuk menampung data user
class User (var id: Int, val name:String, val username: String, val password: String,
            val address: String, val birthday: String, val gender: Int)
