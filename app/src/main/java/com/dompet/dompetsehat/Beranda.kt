package com.dompet.dompetsehat

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.TextView
import com.dompet.dompetsehat.handler.DatabaseHandler
import com.dompet.dompetsehat.util.convertRupiah

// class Beranda adalah turunan class AppCompatActivity
class Beranda : AppCompatActivity() {
    private lateinit var tvCount: TextView
    private lateinit var databaseHandler: DatabaseHandler
    lateinit var sharedPreferences: SharedPreferences
    var username = ""
    var id = 0
    var PREFS_KEY = "prefs"
    var USERNAME_KEY = "username"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beranda)

        tvCount = findViewById(R.id.textViewCount)

        // mengambil data dari login
        sharedPreferences = getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE)
        username = sharedPreferences.getString(USERNAME_KEY, null)!!

        databaseHandler = DatabaseHandler(this)
        id = databaseHandler.viewUser(username).get(0).id
    }

    // function onResume dipanggil sebelum onCreate
    override fun onResume() {
        super.onResume()
        val sumInOut = databaseHandler.sumInOut(id)
        val sumPlanning = databaseHandler.sumPlanning(id)
        val sum = sumInOut - sumPlanning
        val strRupiah = sum.convertRupiah()

        tvCount.text = "Sisa Uang \n" +
                "${strRupiah}"
    }

    // menuju ke halaman pemasukan dan pengeluaran
    fun masuk_keluar_uang(view: View?) {
        val intent = Intent(this@Beranda, Masuk_Keluar_Uang::class.java)
        intent.putExtra("id", id)
        startActivityForResult(intent, 1)
    }

    // menuju ke halaman riwayat keuangan
    fun menu_riwayat_keuangan(view: View?) {
        val intent = Intent(this@Beranda, Riwayat_Keuangan::class.java)
        startActivity(intent)
    }

    // menuju ke halaman manajemen keuangan
    fun menu_manajemen_uang(view: View?) {
        val intent = Intent(this@Beranda, Manajemen_Uang::class.java)
        intent.putExtra("id", id)
        startActivity(intent)
    }

    // menuju ke halaman simpannan
    fun menu_simpanan(view: View?) {
        val intent = Intent(this@Beranda, Simpanan::class.java)
        intent.putExtra("id", id)
        startActivity(intent)
    }

    // menuju ke halaman asset
    fun menu_asset(view: View?) {
        val intent = Intent(this@Beranda, Asset::class.java)
        intent.putExtra("id", id)
        startActivity(intent)
    }

    // menuju ke halaman profil
    fun menu_profil(view: View?) {
        val intent = Intent(this@Beranda, Profil::class.java)
        startActivity(intent)
    }
}