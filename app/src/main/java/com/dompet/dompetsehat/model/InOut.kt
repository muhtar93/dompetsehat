package com.dompet.dompetsehat.model

// class model adalah class yang berfungsi untuk menampung data, class InOut adalah class model
// untuk menampung data pemasukan dan pengeluaran
class InOut (var id: Int, val type:Int, val nominal: Int, val date: String,
             val isTunai: Int, val note: String, val userId: Int)
