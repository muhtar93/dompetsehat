package com.dompet.dompetsehat

import android.app.DatePickerDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.widget.*
import com.dompet.dompetsehat.adapter.AssetAdapter
import com.dompet.dompetsehat.handler.DatabaseHandler
import com.dompet.dompetsehat.model.Asset
import com.dompet.dompetsehat.util.convertRupiah
import java.util.*

// class Asset adalah turunan dari AppCompatActivity
class Asset : AppCompatActivity() {
    // deklarasi variabel
    private lateinit var etDate: EditText
    private lateinit var etType: EditText
    private lateinit var etPrice: EditText
    private lateinit var btnSave: Button
    private lateinit var totalAsset: TextView
    private lateinit var listAsset: ListView
    private lateinit var databaseHandler: DatabaseHandler

    // function onCreate adalah function bawaan dari AppCompatActivity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // layout dari class Asset adalah activity_asset
        setContentView(R.layout.activity_asset)
        databaseHandler = DatabaseHandler(this)

        etDate = findViewById(R.id.dateManage)
        etType = findViewById(R.id.nominalManage)
        etPrice = findViewById(R.id.etNote)
        btnSave = findViewById(R.id.btnSave)
        totalAsset = findViewById(R.id.tvTotalSaving)
        listAsset = findViewById(R.id.listAsset)

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, { view, year, monthOfYear, dayOfMonth ->
            etDate.setText("$year-${monthOfYear + 1}-$dayOfMonth")
        }, year, month, day)

        etDate.setInputType(InputType.TYPE_NULL);
        etDate.setOnClickListener {
            dpd.show()
        }

        btnSave.setOnClickListener {
            addAsset()
        }

        val strRupiah = databaseHandler.sumAsset().convertRupiah()
        totalAsset.text = "Total Asset \n" +
                "${strRupiah}"

        val datas: List<Asset> = databaseHandler.viewAsset()
        val empArrayId = Array<String>(datas.size){"0"}
        val empArrayName = Array<String>(datas.size){"null"}
        val empArrayEmail = Array<String>(datas.size){"null"}
        val arrayType = Array<String>(datas.size){"null"}
        var index = 0
        for(e in datas){
            empArrayId[index] = e.id.toString()
            empArrayName[index] = e.price.toString()
            empArrayEmail[index] = e.date
            arrayType[index] = e.type
            index++
        }

        val myListAdapter = AssetAdapter(this,empArrayId,empArrayName,empArrayEmail, arrayType)
        // menghubungkan list dengan adapter
        listAsset.adapter = myListAdapter
    }

    // function untuk menambah data Asset
    fun addAsset() {
        val id = (1..100).shuffled().last()

        if(etDate.text.trim()!="" && etType.text.trim()!="" &&
            etPrice.text.toString().trim()!=""){
            val status = databaseHandler.addAsset(
                Asset(
                    id,
                    etDate.text.toString(),
                    etType.text.toString(),
                    etPrice.text.toString().toInt(),
                    intent.getIntExtra("id", 0)
                )
            )

            // jika penambahan data berhasil
            if(status > -1){
                Toast.makeText(applicationContext,"Input data berhasil", Toast.LENGTH_LONG).show()
                etDate.text.clear()
                etType.text.clear()
                etPrice.text.clear()
                finish()
            }
        }else{
            // jika penambahan data gagal
            Toast.makeText(applicationContext,"Input data gagal", Toast.LENGTH_LONG).show()
        }
    }
}