package com.dompet.dompetsehat.adapter

import android.app.Activity
import android.content.Intent
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.dompet.dompetsehat.R
import com.dompet.dompetsehat.UpdateManagementActivity
import com.dompet.dompetsehat.handler.DatabaseHandler
import com.dompet.dompetsehat.model.Planning

// class ManagementAdapter adalah turunan dari ArrayAdapter
class ManagementAdapter(private val context: Activity, private val id: Array<String>,
                        private val name: Array<String>, private val date: Array<String>)
    : ArrayAdapter<String>(context, R.layout.manajemen_item, name) {

    // function bawaan dari ArrayAdapter
    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = context.layoutInflater
        val rowView = inflater.inflate(R.layout.manajemen_item, null, true)

        val idText = rowView.findViewById(R.id.tvDate) as TextView
        val nameText = rowView.findViewById(R.id.tvNominal) as TextView
        val delete = rowView.findViewById(R.id.delete) as ImageView
        val edit = rowView.findViewById(R.id.edit) as ImageView

        idText.text = "${date[position]}"
        nameText.text = "${name[position]}"

        val databaseHandler = DatabaseHandler(context)

        // detail manajemen keuangan
        delete.setOnClickListener {
            val status = databaseHandler.deleteManagement(Planning(Integer.parseInt(id[position]),
                "",0, "", 0, 0))
            if(status > -1){
                Toast.makeText(context,"Hapus data berhasil", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(context,"Hapus data gagal", Toast.LENGTH_LONG).show()
            }
        }

        edit.setOnClickListener {
            val intent = Intent(context, UpdateManagementActivity::class.java)
            intent.putExtra("id", Integer.parseInt(id[position]))
            context.startActivity(intent)
        }

        return rowView
    }
}