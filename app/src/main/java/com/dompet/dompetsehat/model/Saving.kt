package com.dompet.dompetsehat.model

// class model adalah class yang berfungsi untuk menampung data, class Saving adalah class model
// untuk menampung data simpanan
class Saving (var id: Int, val date:String, val nominal: Int, val note: String, val userId: Int)
