package com.dompet.dompetsehat.model

// class model adalah class yang berfungsi untuk menampung data, class Asset adalah class model
// untuk menampung data asset
class Asset (var id: Int, val date:String, val type: String, val price: Int, val userId: Int)
