package com.dompet.dompetsehat

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.view.View
import android.widget.*
import com.dompet.dompetsehat.handler.DatabaseHandler
import com.dompet.dompetsehat.model.User
import java.util.*

class Daftar : AppCompatActivity() {
    private lateinit var name: EditText
    private lateinit var username: EditText
    private lateinit var password: EditText
    private lateinit var address: EditText
    private lateinit var birthday: EditText
    private lateinit var register: Button
    private lateinit var spinner: Spinner

    @SuppressLint("SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_daftar)

        register = findViewById(R.id.btnUpdateProfile)
        register.setOnClickListener {
            register()
        }

        val genders = resources.getStringArray(R.array.gender)

        spinner = findViewById(R.id.spinner)
        birthday = findViewById(R.id.tvUpdateBirthday)

        val adapter = ArrayAdapter(this,
            android.R.layout.simple_spinner_item, genders)
        spinner.adapter = adapter

        // memilih drop down
        spinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>,
                                        view: View, position: Int, id: Long) {}
            override fun onNothingSelected(parent: AdapterView<*>) {}
        }

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, { view, year, monthOfYear, dayOfMonth ->
            var months = ""
            if (monthOfYear < 10) {
                months = "0${monthOfYear+1}"
            } else {
                months = "${monthOfYear+1}"
            }

            var day = ""
            if (dayOfMonth < 10) {
                day = "0$dayOfMonth"
            } else {
                day = "$dayOfMonth"
            }
            birthday.setText("$year-$months-$day")
        }, year, month, day)

        birthday.setInputType(InputType.TYPE_NULL);
        birthday.setOnClickListener {
            dpd.show()
        }
    }

    // function untuk memasukann data user ke dalam database
    fun register() {
        name = findViewById(R.id.tvUpdateName)
        username = findViewById(R.id.tvUpdateUsername)
        password = findViewById(R.id.tvUpdatePassword)
        address = findViewById(R.id.tvUpdateAddress)
        val id = (1..100).shuffled().last()

        val databaseHandler = DatabaseHandler(this)
        if(name.text.trim()!="" && username.text.trim()!="" && password.text.trim()!=""
            && address.text.trim()!="" && birthday.text.trim()!=""){
            val status = databaseHandler.addUser(
                User(
                    id,
                    name.text.toString(),
                    username.text.toString(),
                    password.text.toString(),
                    address.text.toString(),
                    birthday.text.toString(),
                    spinner.selectedItemId.toInt()
                )
            )
            if(status > -1){
                Toast.makeText(applicationContext,"Pendaftaran berhasil", Toast.LENGTH_LONG).show()
                name.text.clear()
                username.text.clear()
                password.text.clear()
                address.text.clear()
                birthday.text.clear()
                finish()
            }
        }else{
            Toast.makeText(applicationContext,"id or name or email cannot be blank", Toast.LENGTH_LONG).show()
        }

    }
}