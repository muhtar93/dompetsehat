package com.dompet.dompetsehat.model

// class model adalah class yang berfungsi untuk menampung data, class Planning adalah class model
// untuk menampung data manajemen
class Planning (var id: Int, val date:String, val nominal: Int, val note: String,
                val isDone: Int, val userId: Int)
