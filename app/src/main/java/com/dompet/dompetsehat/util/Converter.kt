package com.dompet.dompetsehat.util

import java.text.NumberFormat
import java.util.*

// function convertRupiah adalah function untuk merubah nilai integer kedalam string rupiah
fun Any.convertRupiah(): String {
    val localId = Locale("in", "ID")
    val formatter = NumberFormat.getCurrencyInstance(localId)
    val strFormat = formatter.format(this)
    return strFormat
}