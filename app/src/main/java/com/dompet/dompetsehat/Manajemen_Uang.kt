package com.dompet.dompetsehat

import android.app.DatePickerDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.widget.*
import com.dompet.dompetsehat.adapter.ManagementAdapter
import com.dompet.dompetsehat.handler.DatabaseHandler
import com.dompet.dompetsehat.model.Planning
import java.util.*

class Manajemen_Uang : AppCompatActivity() {
    private lateinit var editTextDate: EditText
    private lateinit var editTextNominal: EditText
    private lateinit var editTextNote: EditText
    private lateinit var btnSave: Button
    private lateinit var listData: ListView
    private lateinit var databaseHandler: DatabaseHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manajemen_uang)

        databaseHandler = DatabaseHandler(this)

        editTextDate = findViewById(R.id.dateManage)
        editTextNominal = findViewById(R.id.nominalManage)
        editTextNote = findViewById(R.id.etNote)
        btnSave = findViewById(R.id.btnSave)
        listData = findViewById(R.id.listManagement)

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, { view, year, monthOfYear, dayOfMonth ->
            editTextDate.setText("$year-${monthOfYear + 1}-$dayOfMonth")
        }, year, month, day)

        editTextDate.setInputType(InputType.TYPE_NULL);
        editTextDate.setOnClickListener {
            dpd.show()
        }

        btnSave.setOnClickListener {
            addPlanning()
        }

        val datas: List<Planning> = databaseHandler.viewManagement()
        val empArrayId = Array<String>(datas.size){"0"}
        val empArrayName = Array<String>(datas.size){"null"}
        val empArrayEmail = Array<String>(datas.size){"null"}
        var index = 0
        for(e in datas){
            empArrayId[index] = e.id.toString()
            empArrayName[index] = e.nominal.toString()
            empArrayEmail[index] = e.date
            index++
        }
        //creating custom ArrayAdapter
        val myListAdapter = ManagementAdapter(this,empArrayId,empArrayName,empArrayEmail)
        listData.adapter = myListAdapter
    }

    fun addPlanning() {
        val id = (1..100).shuffled().last()

        if(editTextDate.text.trim()!="" && editTextNominal.text.toString().trim()!="" &&
            editTextNote.text.trim()!=""){
            val status = databaseHandler.addPlanning(
                Planning(
                    id,
                    editTextDate.text.toString(),
                    editTextNominal.text.toString().toInt(),
                    editTextNote.text.toString(),
                    0,
                    intent.getIntExtra("id", 0)
                )
            )
            if(status > -1){
                Toast.makeText(applicationContext,"Input data berhasil", Toast.LENGTH_LONG).show()
                editTextDate.text.clear()
                editTextNominal.text.clear()
                editTextNote.text.clear()
                finish()
            }
        }else{
            Toast.makeText(applicationContext,"Input data gagal", Toast.LENGTH_LONG).show()
        }
    }
}