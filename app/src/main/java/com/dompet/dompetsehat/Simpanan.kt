package com.dompet.dompetsehat

import android.app.DatePickerDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.widget.*
import com.dompet.dompetsehat.adapter.HistoryAdapter
import com.dompet.dompetsehat.handler.DatabaseHandler
import com.dompet.dompetsehat.model.Saving
import com.dompet.dompetsehat.util.convertRupiah
import java.util.*

class Simpanan : AppCompatActivity() {
    private lateinit var editTextDate: EditText
    private lateinit var editTextNominal: EditText
    private lateinit var editTextNote: EditText
    private lateinit var btnSave: Button
    private lateinit var btnUsing: Button
    private lateinit var tvTotal: TextView
    private lateinit var listSaving: ListView
    private lateinit var databaseHandler: DatabaseHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_simpanan)

        databaseHandler = DatabaseHandler(this)

        editTextDate = findViewById(R.id.dateManage)
        editTextNominal = findViewById(R.id.nominalManage)
        editTextNote = findViewById(R.id.etNote)
        btnSave = findViewById(R.id.btnSave)
        btnUsing = findViewById(R.id.btnUse)
        tvTotal = findViewById(R.id.tvTotalSaving)
        listSaving = findViewById(R.id.listSaving)

        val strRupiah = databaseHandler.sumSaving().convertRupiah()
        tvTotal.text = "Total Simpanan \n" +
                "${strRupiah}"

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, { view, year, monthOfYear, dayOfMonth ->
            var newMonth =  ""
            if (monthOfYear < 10) {
                newMonth = "0${monthOfYear + 1}"
            } else {
                newMonth = "${monthOfYear + 1}"
            }

            var newDay =  ""
            if (dayOfMonth < 10) {
                newDay = "0${dayOfMonth}"
            } else {
                newDay = "${dayOfMonth}"
            }
            editTextDate.setText("$year-${newMonth}-$newDay")
        }, year, month, day)

        editTextDate.setInputType(InputType.TYPE_NULL)
        editTextDate.setOnClickListener {
            dpd.show()
        }

        btnSave.setOnClickListener {
            addSaving()
        }

        btnUsing.setOnClickListener {
            addUsing()
        }

        val datas: List<Saving> = databaseHandler.viewSaving()
        val empArrayId = Array<String>(datas.size){"0"}
        val empArrayName = Array<String>(datas.size){"null"}
        val empArrayEmail = Array<String>(datas.size){"null"}
        val array = Array<String>(datas.size){"null"}
        var index = 0
        for(e in datas){
            empArrayId[index] = e.id.toString()
            empArrayName[index] = e.nominal.toString()
            empArrayEmail[index] = e.date
            array[index] = e.note
            index++
        }
        //creating custom ArrayAdapter
        val myListAdapter = HistoryAdapter(this,empArrayId,empArrayName,empArrayEmail, array)
        listSaving.adapter = myListAdapter
    }

    fun addSaving() {
        val id = (1..100).shuffled().last()

        if(editTextDate.text.trim()!="" && editTextNominal.text.toString().trim()!="" &&
            editTextNote.text.trim()!=""){
            val status = databaseHandler.addSaving(
                Saving(
                    id,
                    editTextDate.text.toString(),
                    editTextNominal.text.toString().toInt(),
                    editTextNote.text.toString(),
                    intent.getIntExtra("id", 0)
                )
            )
            if(status > -1){
                Toast.makeText(applicationContext,"Input data berhasil", Toast.LENGTH_LONG).show()
                editTextDate.text.clear()
                editTextNominal.text.clear()
                editTextNote.text.clear()
                finish()
            }
        }else{
            Toast.makeText(applicationContext,"Input data gagal", Toast.LENGTH_LONG).show()
        }
    }

    fun addUsing() {
        val id = (1..100).shuffled().last()

        val databaseHandler = DatabaseHandler(this)
        if(editTextDate.text.trim()!="" && editTextNominal.text.toString().trim()!="" &&
            editTextNote.text.trim()!=""){
            val status = databaseHandler.addSaving(
                Saving(
                    id,
                    editTextDate.text.toString(),
                    -editTextNominal.text.toString().toInt(),
                    editTextNote.text.toString(),
                    intent.getIntExtra("id", 0)
                )
            )
            if(status > -1){
                Toast.makeText(applicationContext,"Input data berhasil", Toast.LENGTH_LONG).show()
                editTextDate.text.clear()
                editTextNominal.text.clear()
                editTextNote.text.clear()
                finish()
            }
        }else{
            Toast.makeText(applicationContext,"Input data gagal", Toast.LENGTH_LONG).show()
        }
    }
}