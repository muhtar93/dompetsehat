package com.dompet.dompetsehat.adapter

import android.app.Activity
import android.support.design.widget.BottomSheetDialog
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.dompet.dompetsehat.R
import com.dompet.dompetsehat.handler.DatabaseHandler
import com.dompet.dompetsehat.model.Asset

// class adapter adalah turunan / extend dari class ArrayAdapter
class AssetAdapter(
    private val context: Activity,
    private val id: Array<String>,
    private val name: Array<String>,
    private val date: Array<String>,
    private val type: Array<String>)
    : ArrayAdapter<String>(context, R.layout.list_asset, name) {

    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = context.layoutInflater
        val rowView = inflater.inflate(R.layout.list_asset, null, true)

        val idText = rowView.findViewById(R.id.tvDate) as TextView
        val nameText = rowView.findViewById(R.id.tvNominal) as TextView
        val deleteAsset = rowView.findViewById(R.id.ivDeleteAsset) as ImageView
        val detail = rowView.findViewById(R.id.ivDetail) as ImageView

        idText.text = "${date[position]}"
        nameText.text = "${name[position]}"

        val databaseHandler = DatabaseHandler(context)

        detail.setOnClickListener {
            val dialog = BottomSheetDialog(context)
            val view = context.layoutInflater.inflate(R.layout.bottom_sheet_filter, null)
            dialog.setContentView(view)
            val tvDate = view.findViewById<TextView>(R.id.detailDate)
            val tvNominal = view.findViewById<TextView>(R.id.detailNominal)
            val tvNote = view.findViewById<TextView>(R.id.detailNote)
            tvDate.text = "Tanggal : ${date[position]}"
            tvNominal.text = "Harga : ${name[position]}"
            tvNote.text = "Jenis Item : ${type[position]}"
            dialog.show()
        }


        deleteAsset.setOnClickListener {
            val status = databaseHandler.deleteAsset(
                Asset(Integer.parseInt(id[position]),
                "","", 0, 0)
            )
            if(status > -1){
                Toast.makeText(context,"Hapus data berhasil", Toast.LENGTH_LONG).show()
                databaseHandler.viewAsset()
            } else {
                Toast.makeText(context,"Hapus data gagal", Toast.LENGTH_LONG).show()
            }
        }

        return rowView
    }
}