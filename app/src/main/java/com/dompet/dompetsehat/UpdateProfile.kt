package com.dompet.dompetsehat

// mengimport classs dari library yg sudah terpasang di gradle
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.dompet.dompetsehat.handler.DatabaseHandler
import com.dompet.dompetsehat.model.User

// class UpdateProfile adalah turunan dari class AppCompatActivity
class UpdateProfile : AppCompatActivity() {
    // deklarasi variable
    private lateinit var tvName: EditText
    private lateinit var tvUsername: EditText
    private lateinit var tvPassword: EditText
    private lateinit var tvAddress: EditText
    private lateinit var tvBirthday: EditText
    private lateinit var btnUpdateProfile: Button
    private lateinit var databaseHandler: DatabaseHandler

    // onCreate adalah turunan function dari AppCompatActivity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // memanggil layout yang terdapat pada direktori res/layout
        setContentView(R.layout.activity_update_profile)

        // inisiasi variable
        tvName = findViewById(R.id.tvUpdateName)
        tvUsername = findViewById(R.id.tvUpdateUsername)
        tvPassword = findViewById(R.id.tvUpdatePassword)
        tvAddress = findViewById(R.id.tvUpdateAddress)
        tvBirthday = findViewById(R.id.tvUpdateBirthday)
        btnUpdateProfile = findViewById(R.id.btnUpdateProfile)

        databaseHandler = DatabaseHandler(this)

        // mengambil data login user
        val id = intent.getIntExtra("id", 0)
        val name = intent.getStringExtra("name")
        val username = intent.getStringExtra("username")
        val password = intent.getStringExtra("password")
        val address = intent.getStringExtra("address")
        val birthday = intent.getStringExtra("birthday")
        val gender = intent.getIntExtra("gender", 0)

        // memasukan data login user kedalam view
        tvName.setText(name)
        tvUsername.setText(username)
        tvPassword.setText(password)
        tvAddress.setText(address)
        tvBirthday.setText(birthday)

        // setOnClickListener adalah fungsi untuk klik button update profile
        btnUpdateProfile.setOnClickListener {
            val updateName = tvName.text.toString()
            val updateUsername = tvUsername.text.toString()
            val updatePassword = tvPassword.text.toString()
            val updateAddress = tvAddress.text.toString()
            val updateBirthday = tvBirthday.text.toString()

            // mengecek jangan sampai user memasukan string kosong
            if(updateName.trim()!="" && updateUsername.trim()!=""){
                val status = databaseHandler.updateUser(User(id,updateName,
                    updateUsername, updatePassword, updateAddress, updateBirthday, gender))
                // jika statusnya = 0 maka gagal update profile
                if(status > -1){
                    Toast.makeText(applicationContext,"Profile berhasil diupdate", Toast.LENGTH_LONG).show()
                    val returnIntent = Intent()
                    setResult(RESULT_CANCELED, returnIntent)
                    finish()
                }
            }else{
                Toast.makeText(applicationContext,"id or name or email cannot be blank", Toast.LENGTH_LONG).show()
            }
        }

    }
}